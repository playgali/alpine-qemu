FROM arm32v6/alpine
MAINTAINER Galileo Martinez "playgali@gmail.com"
ENV REFRESHED_AT 2017-Sep-22

ENV QEMU_EXECVE 1
# COPY https://github.com/resin-io-library/base-images/raw/master/alpine/armhf/edge/qemu-arm-static /usr/bin
COPY . /usr/bin

RUN [ "/usr/bin/qemu-arm-static", "/bin/sh", "-c", "ln -s resin-xbuild /usr/bin/cross-build-start; ln -s resin-xbuild /usr/bin/cross-build-end; ln /bin/sh /bin/sh.real" ]
